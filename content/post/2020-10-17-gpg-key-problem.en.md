---
title: GPG key problem
author: Ben Toh
date: '2020-10-17'
slug: gpg-key-problem
categories: []
tags: []
---

Yesterday I ran into this funny problem and thought it's good to write this down. I was updating a Heroku shiny app, which uses a fantastic [Docker Image](https://github.com/virtualstaticvoid/heroku-docker-r). And it throws a cute error:

> `GPG error: https://cloud.r-project.org/bin/linux/ubuntu focal-cran40/ InRelease: The following signatures were invalid: EXPKEYSIG 51716619E084DAB9 Michael Rutter <marutter@gmail.com>`

Sure sounded like the GPG key expired! I then ran into exact same problem in my virtual machine created in [Cyverse's Atmosphere](https://atmo.cyverse.org/application/images).

The GPG key expired on 2020-10-16, how lucky was I?! Because the Docker image was created, and the key was added pre 2020-10-16, the key is no longer valid! But what should the "new key" be? It turns out that the same key now has a new expiry date in 2027, but I needed to remove the key and re-add the same key. For the Docker image, it has been re-built and pushed into the Dockerhub. All good now.

To check your GPG key:

`sudo apt-key list`

This is the "offending" GPG key:

```
/etc/apt/trusted.gpg
--------------------
pub   rsa2048 2010-10-19 [SCA] [expired: 2020-10-16]
      E298 A3A8 25C0 D65D FD57  CBB6 5171 6619 E084 DAB9
uid           [ expired] Michael Rutter <marutter@gmail.com>
```

To remove the key:

`sudo apt-key del "E298 A3A8 25C0 D65D FD57 CBB6 5171 6619 E084 DAB9"`

And then to add it back:

`sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9`

Checking the GPG again and this key now expires in 2027-09-30:

```
/etc/apt/trusted.gpg
--------------------
pub   rsa2048 2010-10-19 [SCA] [expires: 2027-09-30]
      E298 A3A8 25C0 D65D FD57  CBB6 5171 6619 E084 DAB9
uid           [ unknown] Michael Rutter <marutter@gmail.com>
sub   rsa2048 2010-10-19 [E] [expires: 2027-09-30]
```

Something something don't take anything for granted :D