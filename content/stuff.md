---
slug: stuff
title: Stuff that I created
---

Here's a list of stuff that I've created or co-created, presentation that I give or gave, it's growing ;)

- A [Shiny app](http://flcovis19.herokuapp.com/) to visualize Florida's COVID-19 data.
- University of Florida, School of Natural Resources and Environment exit seminar [presentation slides](https://kokbent.github.io/presentations/snre-exit/) where I talk about my PhD research.
- R Tutorials that I created about [purrr package](http://www.r-gators.com/2018/02/28/introduction-to-purrr/) and [creating Shiny app](http://www.r-gators.com/2018/04/04/creating-a-simple-shiny-app-together/).
- American Statistical Association LGBTQ+ Inclusion and Diversity Working Group [website](https://sites.google.com/view/asa-lgbtq/home).
- [Presentation slide](https://kokbent.github.io/presentations/NU/) for job interview with the *Malaria and COVID-19 Modeling at Northwestern* [team](https://www.numalariamodeling.org/).