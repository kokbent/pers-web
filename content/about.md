---
slug: about
title: About Me
---

Malaysian. 13 years in Singapore. Close to 4 year in Gainesville, FL, USA and counting.

Currently a PhD candidate in the Interdisciplinary Ecology program in University of Florida.

I love computer stuff, statistic, ecology and marine stuff. I hope to use statistical model and machine learning to find solutions to all kinds of environmental and public health issues.

[Résumé](../resume.pdf)
[Curriculum vitae](../CV.pdf)